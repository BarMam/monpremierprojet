from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import DataRequired, Email, Length, ValidationError

class RegisterForm(FlaskForm):
    nom = StringField('Nom : ', validators=[DataRequired()])
    Prenom = StringField('Prenom : ', validators=[DataRequired()])

    email = StringField('Email : ', validators=[DataRequired(), Email() ])
    Password = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=4, max=8)])
    submit= SubmitField("Envoyer")

    # Nom : contrôle sur le nombre de caractères
    def validate_nom(form, field):
        if len(field.data) > 15:
            raise ValidationError('First Name must be less than 15 characters')

    # Prenom : contrôle sur le nombre de caractères
    def validate_Prenom(form, field):
        if len(field.data) > 25:
            raise  ValidationError('Last Name must be less 25 characters')




