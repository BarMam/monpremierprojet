from flask import Flask, render_template, redirect, url_for
from forms import RegisterForm
from config import config

app = Flask(__name__)
app.config.from_object(config)

@app.route('/')
def index() :
    return "Bonjour, Bienvenue sur la page d'accueil \n"


@app.route('/register', methods=['get','post'])
def register() :
    form = RegisterForm()

    if form.validate_on_submit():
        print(f'Nom:{form.nom.data}')
        print(f'Prenom:{form.Prenom.data}')
        print(f'Email:{form.email.data}')
       # print(f'epsilon:{form.epsilon.data}')
        # retour sur la page d'accueil
        return redirect(url_for('noise'))

    return render_template('register.html', form=form,  title='Register')

# la page qui permettra d'ajouter du bruit sur les données
@app.route('/noise', methods=['get', 'post'])
def noise():
    form = RegisterForm()

    return render_template('noise.html', form=form, title='Register')




if __name__ == "__main__":
    app.run()